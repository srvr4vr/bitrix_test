<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico" />
<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<div class="mb-hide"><?$APPLICATION->ShowPanel();?></div>
<div class="body-wrapper bg-light" >	

<div class="container-top-header">
<div class="container ">
	<div class="content">
		<div class="col col-mb-5 col-3 col-dt-2 col-ld-3">
			<?
			$frame = new \Bitrix\Main\Page\FrameHelper("auth-area");
			$frame->begin();

			if ($GLOBALS["USER"]->IsAuthorized()) {
				?>
				<div class="top-header-nav">
					<ul class="top-header-nav-ul">
						<li class="parent">
							<a class="top-header-link" href="/personal/profile/"><i class="icon icon-lk"></i><span class="col-mb-hide col-hide col-dt-hide"> Мой профиль</span></a>
							<ul class="second-level">
								<li>
									<a href="/personal/profile/"> Изменить</a>
								</li>
								<li>
									<a href="/?logout=yes">Выйти</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<?
			} else {
				?>
				<a class="top-header-link" href="/auth/"><i class="icon icon-lk"></i><span class="col-mb-hide col-hide col-dt-hide"> Войти</span></a>
				<?
			}
			$frame->end();
			?>
		</div>



	</div> <!-- .content -->
</div> <!-- .container container-top-header -->
</div>



<div class="container">
	<h1 class="main-title">Test you mind</h1>

</div> <!-- .container container-white pt10 -->

<!--	<div class="container container-white pt10">
</div>-->

<div class="container container-main col-margin-top">
<div class="row">
<div class="col-12 col-sm-9 col-margin-bottom">

<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"breadcrumb", 
	array(
		"COMPONENT_TEMPLATE" => "breadcrumb",
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1"
	),
	false
);

?>

<div class="sidebar-header h3"><?$APPLICATION->ShowTitle()?></div>
<div class="card shadow p-3 mb-5 bg-white rounded ">

<!-- df -->

