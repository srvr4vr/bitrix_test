<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)) { ?>
    <div class="social-networks">
        <? foreach($arResult as $arItem) { ?>
            <a href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"]?>" class="social-network"><i class="icon icon-social-<?=$arItem["PARAMS"]["CLASS"]?>"<? if ($arItem["PARAMS"]["CLASS"] == "" && $arItem["PARAMS"]["img"] != "") { ?> style="background-image:url(<?echo $arItem["PARAMS"]["img"]?>);"<? } ?>></i></a>
        <? } ?>
    </div>
<? } ?>
