<?
namespace employers_list\components\classes;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;


//
// в целом косяки те же что и в employers.detail
//
class EmployersList extends \CBitrixComponent
{
	public function executeComponent()
	{
		Loader::includeModule('iblock');
		Loader::includeModule("iblock"); 

		// кеширование
		$cacheTime = $this->arParams['CACHE_TIME'] ?? 3600;
		$cacheAdditionalId = null;
		$cachePath = preg_replace('/[^a-z0-9]/i', '_', __CLASS__);
		if ($this->startResultCache($cacheTime, $cacheAdditionalId, $cachePath)) {
			$this->run();
		}
	}
	
	private function prepareSelection()
	{
		$arSelect = array();
		if($this->isHaveProps()) $arSelect[]="PROPERTY_*";
		$arSelect = array_merge($this->arParams['LIST_FIELD_CODE'], $arSelect);
		return $arSelect;
	}
	
	private function isHaveProps()
	{
		return count($this->arParams["LIST_PROPERTY_CODE"])>0;
	}
	
	//
	// переменные лучше сверху объявлять
	//
	private $application;
	
	private function getProperties(&$obElement)
	{
		$props = $obElement->GetProperties();
		$result = array();
		foreach($this->arParams["LIST_PROPERTY_CODE"] as $pid)
		{
			$prop = $props[$pid];

			if((is_array($prop["VALUE"]) && count($prop["VALUE"])>0)
			  || (!is_array($prop["VALUE"]) && strlen($prop["VALUE"])>0)){
						//$result[$pid] = \CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
						$result[$pid] = $prop["VALUE"];
			}
		}
		return $result;
	}

	public function run()
	{
		$arSelect = $this->prepareSelection();	
		
		$this->arResult["IBLOCK_ID"] = trim($this->arParams["IBLOCK_ID"]);
		$arSort = array();
		$arFilter = array (
			"IBLOCK_ID" => $this->arResult["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"CHECK_PERMISSIONS" => $this->arParams['CHECK_PERMISSIONS'] ? "Y" : "N",);
	
		$res = \CIBlock::GetByID($this->arResult["IBLOCK_ID"]);
		if($ar_res = $res->GetNext())
			$this->arResult["IBLOCK_NAME"]=  $ar_res['NAME'];
		$this->arResult["ITEMS"] = array();
		$rsElement = \CIBlockElement::GetList($arSort, $arFilter , false, false, array(),$arSelect);
		$rsElement->SetUrlTemplates($this->arParams["DETAIL_URL"], "", $this->arParams["IBLOCK_URL"]);

		while($obElement = $rsElement->GetNextElement())
		{
			$arItem = $obElement->GetFields();

			//
			// детали представления, к логике не относятся, не нужны
			//
			$arButtons = \CIBlock::GetPanelButtons(
				$arItem["IBLOCK_ID"],
				$arItem["ID"],
				0,
				array("SECTION_BUTTONS"=>false, "SESSID"=>false)
			);
			$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
			$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];


			Iblock\Component\Tools::getFieldImageData(
				$arItem,
				array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
				Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
				'IPROPERTY_VALUES'
			);
			
			if($this->isHaveProps()){
				$arItem = array_merge($arItem, $this->getProperties($obElement));
			}
			$this->arResult["ITEMS"][] = $arItem;
		}
		$this->includeComponentTemplate();
	}
}
