<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$APPLICATION->SetAdditionalCSS($templateFolder."/css/fontawesome-all.min.css");
$APPLICATION->SetTitle($arResult['IBLOCK_NAME']);
$APPLICATION->AddChainItem($arResult['IBLOCK_NAME']);

//echo $arResult['IBLOCK_NAME'];
//print_r($arResult);
/*foreach($arResult as $item)
{
	print_r($item);
	
	echo "</br>";
	echo "</br>";
	echo "</br>";
}
*/
?>
<div class="row">
<?foreach($arResult['ITEMS'] as $item)
{?>

<div class="col-12 col-xl-3 col-lg-4 col-md-6 col-sm-12 p-0 ">
<a href="<?=$item['DETAIL_PAGE_URL']?>">
	<div class="card shadow  m-1 p-0 h-100">
	<div class="image-placeholder card-img-top" <?if (isset($item['PREVIEW_PICTURE']['SRC'])):?> style="background-image: url(<?=$item['PREVIEW_PICTURE']['SRC'];?>);" <?endif;?> >
		<!--img class="card-img-top" src="	<?=$item['PREVIEW_PICTURE']['SRC'];?>" alt="<?=$item['NAME'];?>"-->
	</div>
		
  <div class="card-body p-1">
  

    <h5 class="card-title text-center text-black"><?=$item['NAME'];?></h5>
	<p class="card-title text-center text-black"><?=$item['DISPLAY_PROPERTIES']['position']['VALUE'];?></p>
	
	
    <? if(isset($item['phone'])):?><p class="card-text m-0 text-center text-black"><i class="fas fa-phone"></i> <?=$item['phone'];?></p><?endif;?>
	<? if(isset($item['email'])):?><p class="card-text m-0 text-center text-black"><i class="fas fa-envelope"></i> <?=$item['email'];?></p><?endif;?>

  </div>
	
	
		
	</div>
	</a>
	
</div>

<?}?>
</div>
