<?
namespace test\components\classes;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;

class StoreComponent extends \CBitrixComponent
{
	private $block_id, $id; 

	public function executeComponent()
	{
		Loader::includeModule('iblock');
		$cacheTime = $this->arParams['CACHE_TIME'] ?? 3600;
		$cacheAdditionalId = null;
		$cachePath = preg_replace('/[^a-z0-9]/i', '_', __CLASS__);
		if ($this->startResultCache($cacheTime, $cacheAdditionalId, $cachePath)) {
			$this->run();
		}
	}
	
	private function getProperties()
	{
		$CDBResult = \CIBlockElement::GetProperty($this->block_id, $this->id);
		$result = array();
		while($prop =$CDBResult->Fetch()){
			if ($prop['MULTIPLE'] == Y) $result[$prop['CODE']][] = $prop['VALUE'];
			 else $result[$prop['CODE']] =$prop['VALUE'];
		}
		return $result;
	}

	public function run()
	{
		$this->id = (int) $this->arParams["ID"];
		$this->block_id = (int) $this->arParams["IBLOCK_ID"];
		
		$ch = curl_init($_SERVER["HTTP_HOST"]."/api/getStoreById.php?BLOCK_ID=".$this->block_id."&ID=".$this->id);
		curl_setopt_array($ch, [
			CURLOPT_RETURNTRANSFER => true,
		]);
		$response = curl_exec($ch);
		curl_close($ch);
		$this->arResult = json_decode($response, true);
		$this->includeComponentTemplate();
	}
}
