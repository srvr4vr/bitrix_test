<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$APPLICATION->SetAdditionalCSS($templateFolder."/css/fontawesome-all.min.css");
?>


<div class="container card shadow item mb-2" id="<?=$arResult['ID']?>" data-name="<?=$arResult['NAME']?>" data-latitude="<?=$arResult['COORDINATES'][0]?>" data-longitude="<?=$arResult['COORDINATES'][1]?>">
	<div class="card-body">
		<h5 class="card-title"><?=$arResult['NAME'];?></h5>
		<h6><?=$arResult['ADDRESS'];?></h6>

		<? if(isset($arResult['PHONES']) && count($arResult['PHONES']) > 0 ):?>

			<?foreach($arResult['PHONES'] as $phone):?>
				<?if(!empty($phone)):?>
					<p class="card-text m-0 text-black"><i class="fas fa-phone"></i><span class="ml-2"><?=$phone;?></span></p>
				<?endif;?>
			<?endforeach;?>		

		<?endif;?>

		<? if(isset($arResult['EMAIL'])):?>
			<p class="card-text m-0 text-black"><i class="fas fa-envelope"></i><a href="mailto:<?=$arResult['EMAIL'];?>"><span class="ml-2"><?=$arResult['EMAIL'];?></span></a></p>
		<?endif;?>
	</div>
</div>