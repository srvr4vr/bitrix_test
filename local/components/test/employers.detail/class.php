<?
namespace employers_list\components\classes;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;

class EmployerDetail extends \CBitrixComponent
{
	public function executeComponent()
	{
		Loader::includeModule('iblock');
		$cacheTime = $this->arParams['CACHE_TIME'] ?? 3600;
		$cacheAdditionalId = null;
		$cachePath = preg_replace('/[^a-z0-9]/i', '_', __CLASS__);
		if ($this->startResultCache($cacheTime, $cacheAdditionalId, $cachePath)) {
			$this->run();
		}
	}

	// 
	// данный метод можно заменить простой проверкой !empty($this->arParams["LIST_PROPERTY_CODE"])
	//
	private function isHaveProps()
	{
		return count($this->arParams["LIST_PROPERTY_CODE"])>0;
	}
	
	//
	// объект  по ссылке можно НЕ передлавать, он автоматически передается по ссылке
	// 
	private function getProperties(&$CDBResult)
	{
		$tempProp = array();
		$result = array();
		while($prop =$CDBResult->Fetch())
		{
			if(in_array($prop['CODE'],$this->arParams["LIST_PROPERTY_CODE"])){
				$tempProp[$prop['CODE']] =$prop['VALUE'];
			}
		}
		return $tempProp;
	}



	public function run()
	{
		//
		// очень странный кусок, т.к.:
		// 1. переопределяются параметры
		// 2. проще/оптимальнее создать перменную $id = (int) $this->arParams["ID"]; и далее только с ней работать
		//
		$this->arParams["ID"] = intval($this->arParams["~ID"]);
		if($this->arParams["ID"] > 0 && $this->arParams["ID"]."" != $this->arParams["~ID"]){
			if (Loader::includeModule("iblock")){
				//
				// PSR! https://www.php-fig.org/psr/psr-2/#6-closures
				//
				Iblock\Component\Tools::process404(
					trim($arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_DETAIL_NF")
					,true
					,$arParams["SET_STATUS_404"] === "Y"
					,$arParams["SHOW_404"] === "Y"
					,$arParams["FILE_404"]
				);
			}
			return;
		}

		// 
		// если предыдущее условие не выполняется, то модуль инфоблока не подключается
		// соответствено данный кусок ошибку выдаст
		// 
		$res = \CIBlockElement::GetByID($this->arParams["ID"]);
		

		// и проверку на 404 логичнее делать тут


		if($ar_res = $res->GetNext()){
			
			Iblock\Component\Tools::getFieldImageData(
				$ar_res,
				array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
				Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
				'IPROPERTY_VALUES'
			);

			if($this->isHaveProps()){
				// 
				// оптимальнее будет получить список нужных свойств, а не получать сначала все, а потом уже отсеивать
				//
				$props  = $this->getProperties(\CIBlockElement::GetProperty($this->arParams["IBLOCK_ID"], $this->arParams["ID"], array("sort" => "asc")));
				//print_r($props);
				//$this->arResult = $ar_res;
				$this->arResult=array_merge($ar_res,$props);
			}
		}
		$this->includeComponentTemplate();
	}
}
