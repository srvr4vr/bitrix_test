<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$APPLICATION->SetAdditionalCSS($templateFolder."/css/fontawesome-all.min.css");
$APPLICATION->SetTitle($arResult['NAME']);
$APPLICATION->AddChainItem($arResult['IBLOCK_NAME'], "../");
$APPLICATION->AddChainItem($arResult['NAME']);
//IBLOCK_NAME

//
// ну и собственно вопрос: зачем нужно было указывать в параметрах компонента, какие то свойства, если тут эта информаиця никак не используется?))))
//

?>

<div class="container row p-0 p-md-1">
	<div class="col-12 col-md-4 p-0 p-md-1">

		<div class="image-placeholder picture" <?if (isset($arResult['DETAIL_PICTURE']['SRC'])):?> style="background-image: url(<?=$arResult['DETAIL_PICTURE']['SRC'];?>);" <?endif;?> ></div>

		<? if(isset($arResult['phone'])):?><p class="card-text m-0 text-center text-black"><i class="fas fa-phone"></i> <?=$arResult['phone'];?></p><?endif;?>
		<? if(isset($arResult['email'])):?><p class="card-text m-0 text-center text-black"><i class="fas fa-envelope"></i><a href="mailto:<?=$arResult['email'];?>"> <?=$arResult['email'];?></a></p><?endif;?>
	
	</div>
	<div class="col-12 col-md-8">
		<h5><?=$arResult['NAME']?></h5>
		<h3><?=$arResult['position']?></h3>
		<?=$arResult['DETAIL_TEXT']?>
	</div>

</div>

такие конструкции:
<a href="mailto:<?=$arResult['email'];?>"> <?=$arResult['email'];?><a>

локаничнее менять:
<?= "<a href='mailto:{$arResult['email']}'>{$arResult['email']}</a>" ?>

и пожалуйста, чуть больше отсутпов, не жалей переносы строк)))
и лучше сделать местами дублирование кода, но чтобы было проще для восприятия

<div class="container row p-0 p-md-1">
	<div class="col-12 col-md-4 p-0 p-md-1">

		<?if (isset($arResult['DETAIL_PICTURE']['SRC'])):?>
			<div class="image-placeholder picture" style="background-image: url(<?=$arResult['DETAIL_PICTURE']['SRC'];?>);"></div>
		<?php else: ?>
			<div class="image-placeholder picture"></div>
		<?endif;?>

		<? if(isset($arResult['phone'])):?>
			<p class="card-text m-0 text-center text-black">
				<i class="fas fa-phone"></i> <?=$arResult['phone'];?>
			</p>
		<?endif;?>

		<? if(isset($arResult['email'])):?>
			<p class="card-text m-0 text-center text-black">
				<i class="fas fa-envelope"></i> <a href="mailto:<?=$arResult['email'];?>"> <?=$arResult['email'];?></a>
			</p>
		<?endif;?>
	
	</div>
	<div class="col-12 col-md-8">

		и тут логичнее H3 сделать имя, а H5 должность
		эти тэги акцентируют информаицю для поисковика (да и для пользователя по хорошему)

		<h5>
			<?=$arResult['NAME']?>
		</h5>
		<h3>
			<?=$arResult['position']?>
		</h3>
		<?=$arResult['DETAIL_TEXT']?>
	</div>

</div>