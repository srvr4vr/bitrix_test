<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes();

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];

$arMaps = array(
   "google" => "Google Maps",
   "yandex" => "Карты Yandex",
);

$arComponentParameters = array(
	"GROUPS" => array(
		"MAP_SETTINGS" => array(
			"NAME" =>"Настройки карты",
			"SORT" => "50"
		)
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_IBLOCK_DESC_LIST_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_IBLOCK_DESC_LIST_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '={$_REQUEST["ID"]}',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"CACHE_TIME" => array(
			"PARENT" => "BASE",
			"NAME" => 'Время кеширования (сек.):',
			"TYPE" => "STRING",
			"DEFAULT" => '3600',
		),
		"MAP_TYPE" =>  array(
			"PARENT" => "MAP_SETTINGS",
			"NAME" => "Тип карты: ",
			"TYPE" => "LIST",
			"VALUES" => $arMaps,
			"DEFAULT" => 'yandex',
			"MULTIPLE" => "N",
		),

		"MAP_CENTER_LATITUDE" => array(
			"PARENT" => "MAP_SETTINGS",
			"NAME" => 'Широта центра карты:',
			"TYPE" => "STRING",
			"DEFAULT" => '55.44103330378191',
		), 
		"MAP_CENTER_LONGITUDE" => array(
			"PARENT" => "MAP_SETTINGS",
			"NAME" => 'Долгота центра карты:',
			"TYPE" => "STRING",
			"DEFAULT" => '65.338724359375',
		), 
	),
);
?>

