<?
namespace test\components\classes;

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);

use Bitrix\Main\Loader,
	Bitrix\Main,
	Bitrix\Iblock;

class StoresListComponent extends \CBitrixComponent
{
	private $block_id; 

	public function executeComponent()
	{
		Loader::includeModule('iblock');
		$cacheTime = $this->arParams['CACHE_TIME'] ?? 3600;
		$cacheAdditionalId = null;
		$cachePath = preg_replace('/[^a-z0-9]/i', '_', __CLASS__);
		if ($this->startResultCache($cacheTime, $cacheAdditionalId, $cachePath)) {
			$this->run();
		}
	}
	
	public function run()
	{
		$this->block_id = (int) $this->arParams["IBLOCK_ID"];
		$ch = curl_init($_SERVER["HTTP_HOST"]."/api/getStoreList.php?BLOCK_ID=".$this->block_id."&mode=ids");
		curl_setopt_array($ch, [
			CURLOPT_RETURNTRANSFER => true,
		]);
		$response = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($response);

		$this->arResult["ITEMS"]=$result;
		
		$this->includeComponentTemplate();
	}
}
