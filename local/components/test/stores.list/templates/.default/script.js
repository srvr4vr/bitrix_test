
var mapType;
var myMap;
var zoom = 12;
var centerPoint;
var uri;

$(document).ready(documentReady);

function documentReady(){

	initVars();
	initMap();
  	$(".item").each(function() {
		$(this).click(onItemClick);
	});
}

function initMap(){
	if (mapType=="yandex"){
		initYandex();
	}else{
		initGooglemaps();
	}
}

function initVars()
{
	var lat = parseFloat($("#map").data("centerLat"));
	var lon = parseFloat( $("#map").data("centerLon"));
	centerPoint = [lat,lon];
	mapType = $("#map").data("type");
	var root = location.protocol + '//' + location.host;
	uri = root+ '/api/getStoreList.php?BLOCK_ID=' + blockId + "&callback=getStoresCallback";
}

function initGooglemaps()
{
	myMap = new google.maps.Map(document.getElementById('map'), {
        center: {lat: centerPoint[0], lng: centerPoint[1]},
        zoom: zoom
    });

	var script = document.createElement('script');
	script.src = uri;
	document.getElementsByTagName('head')[0].appendChild(script);
}

function getStoresCallback(response) {
	for (var i = 0; i < response.features.length; i++) {
      	var coords = response.features[i].geometry.coordinates;
	
        var latLng = new google.maps.LatLng(coords[0],coords[1]);
        var marker = new google.maps.Marker({
    	    position: latLng,
        });
		marker.set('id',response.features[i].id)
		marker.setMap(myMap);
		marker.addListener('click', function(e){
			changeSelection(this.id);
			console.log(this.getPosition());
			myMap.panTo(this.getPosition());
		});
    }
}

function selectItem(id){
	$("#"+id+".item").addClass("selected");
}

function changeSelection(id){
	clearHighlight();
	selectItem(id);
}

function initYandex (){
	ymaps.ready(function()
	{
		myMap = new ymaps.Map('map', {
            	center: centerPoint,
            	zoom: zoom
        	});
	
		var loadingObjectManager = new ymaps.LoadingObjectManager(uri,
			{
				geoObjectOpenBalloonOnClick: false
			});
		myMap.geoObjects.add(loadingObjectManager);

		loadingObjectManager.objects.events.add('click',  function(e){
			changeSelection(e.get('objectId'));
			myMap.panTo(e.get('coords'));
			//myMap.panTo();
		});
	});
}	 


function clearHighlight(){
	$(".selected").each(function(){
		$(this).removeClass("selected");
	});
}

function getCoordinate(latitude, longitude, mapType){
	return (mapType=="yandex")
		? [latitude, longitude] 
		: {lat: latitude, lng: longitude};
}

function onItemClick(){
	var latitude =$( this ).data('latitude'); 
	var longitude =$( this ).data('longitude'); 
	var coordinate = getCoordinate(latitude,longitude, mapType);
	clearHighlight();
	$(this).addClass("selected");
	myMap.panTo(coordinate);
}


