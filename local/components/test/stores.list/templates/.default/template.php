<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Page\Asset;

if ($arParams['MAP_TYPE'] == "yandex")
	Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
else
	Asset::getInstance()->addJs("http://maps.google.com/maps/api/js?key=AIzaSyCWc1KJpqGiDLLjzoFIpOxkLPbAlQjdkPM");



/*$coordinates = array_reduce($arResult['ITEMS'], function($carry, $item) {  
    $carry[]= $item['COORDINATES'];
    return $carry; 
}, array()); */
?>
<!--script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script-->



<script type="text/javascript">
	var blockId = <?=$arParams['IBLOCK_ID'];?>;
</script>

<div class="container">
	<div class="row">
		<div class="col-sm-4 col-12">
			<?foreach($arResult['ITEMS'] as $storeId):?>
				<?$APPLICATION->IncludeComponent(
					"test:store", 
					".default", 
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_TYPE" => "stores",
						"IBLOCK_ID" => $arParams['IBLOCK_ID'],
						"ID" => $storeId,
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "3600"
					),
					false
				);?>
			<?endforeach;?>		
		</div>
		
		<div class="col-sm-8 col-12" id="map" data-type="<?=$arParams['MAP_TYPE'];?>" data-center-lat="<?=$arParams['MAP_CENTER_LATITUDE'];?>" data-center-lon="<?=$arParams['MAP_CENTER_LONGITUDE'];?>">
	
		</div>
		
	</div>
</div>