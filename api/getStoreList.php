<?
header('Content-Type: application/json; charset=utf-8');
use Test\Stores;

require_once(__DIR__."/classes/Stores.php");

$stores = new Test\Stores\Stores($_GET['BLOCK_ID']);
echo (isset($_GET['mode']) && $_GET['mode']=="ids")
            ? $stores->GetStoresIds()
            : $stores->GetStoreList();
?>