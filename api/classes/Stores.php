<?
namespace Test\Stores;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



use Bitrix\Main\Loader,
    Bitrix\Main,
    Bitrix\Iblock;
	
class Stores
{
    private $block_id; 
    private $arFilter; 
    
    function __construct($id) {

        Loader::includeModule('iblock');
        $this->block_id = $id;
        $this->arFilter = array (
			"IBLOCK_ID" => $id,
			"ACTIVE" => "Y",
		);
    }

    public function GetStoreList()
    {
        $arSelect= array("ID", "NAME", "PROPERTY_COORDINATES","PROPERTY_ADDRESS", );
		$arResult = array();
		$rsElement = \CIBlockElement::GetList(array(), $this->arFilter , false, false, array(), $arSelect);
  
		while($obElement = $rsElement->GetNextElement()){
			$arItem = $obElement->GetFields();
           
            $resultItem["type"]="Feature";
			$resultItem["id"]= $arItem["ID"];

            
            $coordString = $obElement->GetProperty("COORDINATES")['VALUE'];


            $coordianates =array_reduce(explode(",", $coordString), function($carry, $item) { 
                $carry[]=floatval($item);
                return $carry;
            }, array());
            //floatval 
            $resultItem["geometry"]= array("type"=>"Point","coordinates"=>$coordianates);
            $resultItem["properties"] = array(
                "hintContent"=>$arItem["NAME"]);    

			$arResult[] =$resultItem;
        }

        $test =array();
        $test["type"]="FeatureCollection";
        $test["features"] = $arResult;



        echo isset($_GET['callback'])
            ? "{$_GET['callback']}(".json_encode($test).")"
            : json_encode($test);

    }

    private function getProperties($id)
	{
        $CDBResult = \CIBlockElement::GetProperty($this->block_id, $id);
        
        $result = array();

		while($prop =$CDBResult->Fetch()){
            
			if ($prop['MULTIPLE'] == Y) $result[$prop['CODE']][] = $prop['VALUE'];
			elseif ($prop['CODE']=="COORDINATES") {
                $result[$prop['CODE']] = explode(",", $prop['VALUE']);
            }else
                $result[$prop['CODE']] =$prop['VALUE'];
		}
		return $result;
	}

    public function GetStoreById($id)
    {
        $res = \CIBlockElement::GetByID($id);
        $result=array();
		if($ar_res = $res->GetNext()){
            
            $result["ID"] = $ar_res["ID"];
            $result["NAME"]= $ar_res["NAME"];
            $props  = $this->getProperties($id);
            echo json_encode(array_merge($result,$props));	
		}

    }

    public function GetStoresIds()
    {
        $arSelect= array("ID");
		$arResult = array();
		$rsElement = \CIBlockElement::GetList(array(), $this->arFilter , false, false, array(), $arSelect);
		
		while($obElement = $rsElement->GetNextElement()){
        //    print_r($obElement);
            $arItem = $obElement->GetFields();
			$arResult[]= $arItem['ID'];
        }
        echo json_encode($arResult);
    }
}
?>